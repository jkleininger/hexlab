

package net.qrab.hexlab;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;


class MainPanel extends JPanel {

  private final Board myBoard = new Board();

  public MainPanel() {
    super();

    add(myBoard);


    this.setFocusable(true);
    this.setPreferredSize(new Dimension(650,650));
    this.setOpaque(true);
    this.setBackground(Color.WHITE);
    this.setVisible(true);
    this.validate();

  }

  public void update() {
    myBoard.update();
  }

}
