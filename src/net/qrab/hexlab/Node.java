package net.qrab.hexlab;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Point2D;

public class Node extends Rectangle {

  Shape  myFillShape;
  Shape  myBorderShape;
  Color  myFill   = new Color(0x332233);
  Color  myBorder = new Color(0x886688);
  double borderThickness = 0.01;
  double xpos,ypos;
  double radius;

  public Node() {
    this(0,0);
  }

  public Node(int col, int row) {
    super(col,row,1,1);
    radius        = 1;
    myFillShape   = unitHex(radius * (1-borderThickness));
    myBorderShape = unitHex(radius);
    setLocation(col,row);
  }

  @Override
  public void setLocation(int col, int row) {
    //axial coordinates
    xpos = ((double)col*Math.sqrt(3)*radius) + ((radius * Math.sqrt(3) / 2) * (double)row);
    ypos = (double)row * (1.5 * radius);
    //odd row offset
    //xpos = ((col + 0.5 + ((double)(row&1)/2)) * Math.sqrt(3)) * radius;
    //ypos = ((double)row * (1.5 * radius)) + radius;
  }

  public double distance(double px, double py) {
    return distance(new Point2D.Double(px,py));
  }

  public double distance(Point2D p) {
    return p.distance(xpos * Hexlab.scale,ypos * Hexlab.scale);
  }

  public double distance(Node n) {
    return new Point2D.Double(n.xpos,n.ypos).distance(xpos*Hexlab.scale, ypos*Hexlab.scale);
  }

  private static Shape unitHex(double radius) {
    double xs[] = new double[6];
    double ys[] = new double[6];
    double theta;

    for(int p=0 ; p<6 ; p++) {
      theta = (Math.PI / 6) * ((2 * p) + 1);
      xs[p] = radius * Math.cos(theta);
      ys[p] = radius * Math.sin(theta);
    }

    return Common.makePath( xs, ys );
  }

  public void update() {
  }

  public void paint(Graphics2D g) {
    g.setColor(myFill);
    g.fill(   Common.scaleArea(myBorderShape,Hexlab.scale,xpos,ypos)   );
    g.setColor(myBorder);
    g.draw(   Common.scaleArea(myBorderShape,Hexlab.scale,xpos,ypos)   );

    //g.setColor(myBorder);
    //g.fill(   Common.scaleArea(myBorderShape,Hexlab.scale,xpos,ypos)   );
    //g.setColor(myFill);
    //g.fill(   Common.scaleArea(myFillShape,Hexlab.scale,xpos,ypos)   );
  }

}
