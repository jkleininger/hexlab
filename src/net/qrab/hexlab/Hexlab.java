package net.qrab.hexlab;

import java.util.Random;
import javax.swing.JFrame;

public class Hexlab {

  protected static final int boardWd    = 40;
  protected static final int boardHt    = 30;
  protected static final int numOfRooms = 300;
  protected static double    scale      = 20;

  protected static           Random rand = new Random();

  private static             MainPanel content;
  private static final       JFrame theFrame = new JFrame("Hexlab");

  private   static boolean   isRunning = false;

  protected static final int N = 0x08;
  protected static final int E = 0x04;
  protected static final int S = 0x02;
  protected static final int W = 0x01;

  public static void main(String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
       @Override
       public void run() {
          content = new MainPanel();
          theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          theFrame.setContentPane(content);
          theFrame.pack();
          theFrame.setVisible(true);
       }
    });

    isRunning = true;
    Thread theThread = new Thread() {
      @Override
      public void run() {
        theLoop();
      }
    };
    theThread.start();
  }

  private static void theLoop() {
    while (isRunning) {
      theFrame.getContentPane().requestFocusInWindow();
      theFrame.getContentPane().repaint();
    }
  }


}
