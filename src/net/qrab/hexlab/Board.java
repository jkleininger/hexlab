package net.qrab.hexlab;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JPanel;

public class Board extends JPanel {

  int wd, ht;
  int xOffset, yOffset;

  ConcurrentHashMap<Integer, Node> nodes = new ConcurrentHashMap<>();

  protected Board() {
    super();

    wd = 10;
    ht = 10;
    xOffset  = 5;
    yOffset  = 5;

    for (int c = 0; c < wd; c++) {
      for (int r = 0; r < ht; r++) {
        nodes.put(linearize(c, r), new Node());
        nodes.get(linearize(c,r)).setLocation(c,r);
      }
    }

    this.setFocusable(true);
    //this.setPreferredSize(new Dimension(wd*(int)Hexlab.scale+1,ht*(int)Hexlab.scale+1));
    this.setPreferredSize(new Dimension(600, 600));
    this.setOpaque(true);
    this.setBackground(Color.BLACK);
    this.setVisible(true);
    this.validate();

    this.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent me) {
        doClick(me);
      }
    });

  }

  private Node[] neighbors(Node theNode) {
    int i = theNode.x;
    int j = theNode.y;
    return new Node[] {
      theNode,
      nodes.get( linearize(i  , j-1) ),
      nodes.get( linearize(i+1, j-1) ),
      nodes.get( linearize(i+1, j  ) ),
      nodes.get( linearize(i  , j+1) ),
      nodes.get( linearize(i-1, j+1) ),
      nodes.get( linearize(i-1, j  ) )
    };
  }

  private int linearize(int c, int r) {
    return (r * wd) + c;
  }

  public void update() {
  }

  public void doClick(MouseEvent e) {
    for(Node thisNode : nodes.values() ) {
      thisNode.myFill = Color.BLACK;
    }
    nodeAt(e.getX(), e.getY()).myFill = new Color(0x555533);
  }

  protected Node nodeAt(double mx, double my) {
    Node retNode;
    double mq = ((Math.sqrt(3) * mx) - my) / (3 * Hexlab.scale);
    double mr = (2 * my) / (3 * Hexlab.scale);

    retNode = nodes.get( linearize((int)mq,(int)mr) );
    if(retNode!=null) {

      double minDelta = Double.MAX_VALUE;
      //for(Node thisNode : neighbors(retNode)) {
      for(Node thisNode : nodes.values()) {
        if(thisNode!=null) {
          double thisDelta = thisNode.distance(mx,my);
          if(thisDelta<minDelta) {
            minDelta = thisDelta;
            retNode = thisNode;
          }
        }
      }

    }
    return retNode;
  }



  @Override
  public void paintComponent(Graphics gfx) {
    super.paintComponent(gfx);
    Graphics2D g = (Graphics2D) gfx;

    for (Node hn : nodes.values()) {
      hn.paint(g);
    }

  }

}
